<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mail\CorreoNota;
use App\Mail\NotaEliminadaAdm;
use App\Http\Requests\NotaValidation;

use Mail;
use App\User;
Use App\Vote;
use App\Note;

class NotasController extends Controller
{

    protected $note;
   
    protected $vote;

    //constructor (intancia entidades)
    public function __construct(Note $note, Vote $vote)
    {
        $this->note = $note; 
        $this->vote = $vote;
    }

    //Crear Nota vista
    function VistaCrearNota(){

    	return view('Note.CrearNota');
    }

    //insertar una nueva nota y envia un correo al usuario que hizo una nueva nota
    function InsertarNota(request $request , NotaValidation $request2){

        $this->validate($request2,[
            'title' => 'required|max:255|min:1',
            'content' => 'required|max:255|min:6',
            'clasification' => 'required|max:255|min:1',
            'comment' => 'max:255|min:0',

        ]);


        $note = Note::create($request->all());


        if($request->email_value){
                Mail::to(Auth::user()->email)->send(new CorreoNota($note));
            }
        return redirect('/nota/propias')->withErrors(['Haz Creado una nueva nota']);

    }

    //Consultar todas las notas -
    function ConsultarNotas()
    {
        $notas = Note::where('view','Publica')->paginate(5);
        return view('notasPublicas')->with('notas',$notas);
    }


    function Index()
    {
        return view('Index');
    }


    //Filtro de nombre en las notas 
    function ConsultaFiltrada(request $request)
    { 
        $notas = Note::whereHas('User', function ($query) use ($request) {
        $query->where('name', 'like', "%{$request->filtro_name}%");
        })->orWhere('title','like',"%{$request->filtro_name}%")->paginate(5);

        return view('notasPublicas')->with('notas',$notas);
    }


    //Editar una nota
	function VistaEditarNota(request $request)
    {
        $notas = Note::all()->where('id',$request->id_nota);
        return view('Note.EditarNota')->with('notas',$notas)->with('UserId',Auth::user()->id);
	}

    //Actualizado de una nota // areglar this update
	function ActualizarNota(request $request, NotaValidation $request2)
    {

        $this->validate($request2,[
            'title' => 'required|max:255|min:1',
            'content' => 'required|max:255|min:6',
            'clasification' => 'required|max:255|min:1',
            'comment' => 'max:255|min:0',
        ]);
        $nota = Note::where('id',$request->id_nota)->first();
        $nota->content = $request->content;
        $nota->clasification = $request->clasification;
        $nota->comment = $request->comment;
        $nota->view = $request->view;
        $nota->save();
		return redirect('/nota/propias')->withErrors(['Haz actualizado una nota']);
    }

        //Vista eliminar nota, toma los datos de la nota que se eliminara
	function VistaEliminarNota(request $request)
    {
        $notas = Note::all()->where('id','=',$request->id_nota);
        return view('Note.EliminarNota')->with('notas',$notas)->with('UserId',Auth::user()->id);
	}

        //Elimina la nota
	function EliminarNota(request $request)
    {	
        $votes = Vote::where('note_id',$request->id_nota)->delete();
        $nota = Note::where('id',$request->id_nota)->delete();
		return redirect('/nota/propias')->withErrors(['Haz Eliminado una nota']);
	}

    //Consulta a  las notas propias
    function ConsultarNotasPropias()
    {
        $notas = Note::where('user_id',Auth::user()->id)->paginate(5);
        return view('Note.notasPropias')->with('notas',$notas)->with('UserId',Auth::user()->id);
    }

        //Ordenar todas las notas 
    function OrdenarNotas(request $request){
        try
        {             
            
            $notas = Note::join('users','user_id','=','users.id')->orderBy($request->type, $request->form)->paginate(5,['notes.*']);
            return view('notasPublicas')->with('notas',$notas);
            
        }
        catch(\Exception $e)
        {
            abort(500);
        }
    }

    //Votos
    function vote(request $request)
    {
        try
        {    
            // dd($request->all());
            $vote = $this->vote->where([
                ['user_id', '=', $request->user_id],
                ['note_id', '=', $request->note_id]
            ])->first();

            //comprobar que no haya dado like a la publicacion
            if(!$vote)
            {
                Vote::create($request->all());
                return redirect(url()->previous().'#'.$request->note_id)->withErrors([$request->note_id =>'Haz votado por la nota']);
            } 
            else 
            {
                if($vote->vote_type == 'Dislike' & $request->vote_type == 'Like' |  $request->vote_type == 'Dislike' & $vote->vote_type == 'Dislike')
                {      
                    if($request->vote_type == 'Like')
                    {
                        $vote->vote_type = 'Like';
                        $vote->save();
                    }
                    else
                    {
                        $vote->delete();
                    }
                }
                else 
                    if($vote->vote_type=='Like' & $request->vote_type == 'Dislike' | $request->vote_type == 'Like' & $vote->vote_type == 'Like') 
                    {    
                        if($request->vote_type == 'Dislike')
                        {
                            $vote->vote_type = 'Dislike';
                            $vote->save();
                        }
                        else
                        {
                            $vote->delete();
                        }
                    }
                return redirect(url()->previous().'#'.$request->note_id)->withErrors([$request->note_id =>'Haz deshecho tu voto anterior']);
            }
        }
        catch(\Exception $e)
        {  
            abort(500);
        }
    }
}
