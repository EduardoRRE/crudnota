<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotaValidation extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255|min:1',
            'content' => 'required|max:255|min:6',
            'clasification' => 'required|max:11|min:1',
            'comment' => 'min:0|max:255',
            //
        ];
    }

    public function messages(){

        return [

        'title.required' => 'El campo :attribute es requerido',
        'title.max' => 'El maximo de caracteres permitidos son 255 en el campo :attribute',
        'title.min' => 'El minimo de caracteres permitidos es de 1 en el campo :attribute',

        'content.required' => 'El campo :attribute es requerido',
        'content.max' => 'El maximo de caracteres permitidos son 255 en el campo :attribute',
        'content.min' => 'El minimo de caracteres permitidos es de 6 en el campo :attribute',

        'clasification.required' => 'El campo :attribute es requerido',
        'clasification.max' => 'El maximo de caracteres permitido es de 10 en el campo :attribute',
        'clasification.min' => 'El minimo de caracteres permitido es de 1 en el campo :attribute',

        'comment.max' => 'El maximo de caracteres permitido es de 255 caracteres en el campo :attribute'
        
        ];
    }


    public function attributes()
    {
        return[
            'title' => 'Titulo',
            'content' => 'Contenido',
            'clasification' => 'Clasificacion',
            'comment' => 'Comentario'
        ];
    }
}
