<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Note;

class CorreoNota extends Mailable
{
    use Queueable, SerializesModels;

    public $correonota;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Note $Notarecibida)
    {
        $this->correonota = $Notarecibida;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.CorreoNota')->with('contenido',$this->correonota->content)->with('clasificacion',$this->correonota->clasification)->with('comentario',$this->correonota->comment);
    }
}
