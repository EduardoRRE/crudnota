<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //
    protected $table = 'notes';
    protected $fillable = [
        'title',
    	'content',
    	'clasification',
    	'comment',
        'view',
    	'user_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function countLikes()
    {
        return $this->votes()->where('vote_type','Like')->count();
    }

    public function countDislikes()
    {
        return $this->votes()->where('vote_type','Dislike')->count();
    }

}
