<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    //
    protected $table = 'votes';
    protected $fillable = [
    	'note_id',
    	'user_id',
        'vote_type'
    ];


    public function user(){
    	return $this->belongToMany('App\User');
    }

    public function notes(){
    	return $this->belongsTo('App\Note');
    }
}
