@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<p>
					<div class="card">
						<div class="card-header">Crear una nueva nota</div>
						<div class="card-body">
							@foreach ($errors->all() as $error)
						    	<li>{{ $error }}</li>
							@endforeach
							{!! Form::open(['route' => 'nota.insertar', 'method' => 'post']) !!}
							<p>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										Titulo
									</label>
									<div class="col-md-6">
										{!! Form::text('title',null,array('required' => 'required','placeholder'=> 'Titulo de la nota')) !!}
									</div>
								</div>
							</p>
							<p>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										Contenido
									</label>
									<div class="col-md-6">
										{!! Form::text('content',null,array('required' => 'required', 'class' => 'inputcontent','placeholder'=> 'Contenido de la nota')) !!}
									</div>
								</div>
							</p>
							<p>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										Clasificacion
									</label> 
									<div class="col-md-6">
										{!! Form::select('clasification', array(
										'Importante' => 'Importante',
										'Media' => 'Media',
										'Baja' => 'Baja'
										)) !!}
									</div>
								</div>
							</p>
							<p>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										Tipo de nota
									</label>
									<div class="col-md-6">
										{!! Form::select('view', array('Publica' => 'Publica', 'Privada' => 'Privada')) !!}
									</div>
								</div>
							</p>
							<p>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										Comentario(Opcional)
									</label> 
									<div class="col-md-6">
										{!! Form::text('comment',null,array('placeholder'=>'Comentario')) !!}
									</div>
								</div>
							</p>
							<p>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										Recibir email
									</label> 
									<div class="col-md-6">
										{!! Form::checkbox('email_value','true',true) !!}
									</div>
								</div>
							</p>
							<p>
								<div class="form-group row">
									<label for="password" class="col-md-9 col-form-label text-md-right">
										{!! Form::submit('Crear',['class' => 'btn btn-primary']) !!}
										{!! Form::hidden('user_id',Auth::user()->id) !!}
									</label> 
								</div>
							</p>
							{!! Form::close() !!}
						</div>
					</div>
				</p>
			</div>
		</div>
	</div>

@endsection