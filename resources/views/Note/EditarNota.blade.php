@extends('layouts.app')
@section('content')
	@foreach($notas as $nota)
		@if($nota->user_id != $UserId)
			<script>
 				window.location.href = '/'; //using a named route
			</script>
		@endif
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<p>
						<div class="card">
							<div class="card-header">Editar Nota</div>
							<div class="card-body">
								@foreach ($errors->all() as $error)
				    			<li>{{ $error }}</li>
				   				@endforeach
				   				{!! Form::open(['route' => 'nota.editar.actualizar', 'method' => 'post']) !!}
				   				{!! Form::hidden('id_nota',$nota->id) !!}
				   				<p>
				   					<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
											Titulo
										</label> 
										<div class="col-md-6">
											{!! Form::text('title',$nota->title,array('required' => 'required','placeholder'=> 'Titulo de la nota')) !!}
										</div>
									</div>
				   				</p>
				   				<p>
				   					<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
											Contenido
										</label> 
										<div class="col-md-6">								
											{!! Form::text('content',$nota->content,array('class'=>'inputcontent')) !!}
										</div>
									</div>
				   				</p>
				   				<p>
				   					<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
											Clasificacion
										</label> 
										<div class="col-md-6">
											{!! Form::select('clasification',array(
												'Imporntante' => 'Importante',
												'Media' => 'Media',
												'Baja ' => 'Baja'
											)) !!}
										</div>
									</div>
				   				</p>
				   				<p>
				   					<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
											Tipo de nota
										</label>
										<div class="col-md-6">
											{!! Form::select('view', array('Publica' => 'Publica', 'Privada' => 'Privada')) !!}
										</div>
									</div>
				   				</p>
				   				<p>
				   					<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
											Comentario
										</label> 
										<div class="col-md-6">								
											{!! Form::text('comment', $nota->comment) !!}
										</div>
									</div>
				   				</p>
				   				<p>
				   					<div class="form-group row">
										<label for="password" class="col-md-9 col-form-label text-md-right">								
											{!! Form::submit('Editar',['class' => 'btn btn-primary']) !!}
										</label> 							
									</div>
				   				</p>
				   				{!! Form::close() !!}
							</div>
						</div>
					</p>
				</div>
			</div>
		</div>
	@endforeach
@endsection