@extends('layouts.app')

@section('content')
	@foreach($notas as $nota)
		@if($nota->user_id != $UserId)
			<script>
		 		 window.location.href = '/';
			</script>
		@endif
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<p>
						<div class="card">
							<div class="card-header">
								Eliminar Nota
							</div>
							<div class="card-body">
								{!! Form::open(['route' => 'nota.eliminada', 'method' => 'delete']) !!}
								{!! Form::hidden('id_nota',$nota->id) !!}
								<p>
									<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
											Titulo
										</label>
										<div class="col-md-6">
											{!! Form::text('title',$nota->title,array('readonly' => 'true')) !!}
										</div>
									</div>
								</p>
								<p>
									<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
											Contenido
										</label> 
										<div class="col-md-6">
											{!! Form::text('contenido_nota',$nota->content,array('class'=>'inputcontent','readonly' => 'true')) !!}
										</div>
									</div>
								</p>
								<p>
									<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
											Clasificacion
										</label> 
										<div class="col-md-6">
											{!! Form::text('clasificacion_nota',$nota->clasification,array('readonly'=>'true')) !!}
										</div>
									</div>
								</p>
								<p>
									<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">
										Comentario
										</label> 
										<div class="col-md-6">
											{!! Form::text('comentario_nota',$nota->comment,array('readonly'=>'true')) !!}
										</div>
									</div>
								</p>
								<p>
									<div class="form-group row">
										<label for="password" class="col-md-9 col-form-label text-md-right">								
											{!! Form::submit('Eliminar',['class' => 'btn btn-danger']) !!}
										</label> 
									</div>
									{!! Form::close() !!}
								</p>
							</div>
						</div>
					</p>
				</div>
			</div>
		</div>
	@endforeach
@endsection