@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="card">
            <div class="buscador"> 
                <center>
                    {!! Form::open(['route' => 'nota.filtro', 'method' => 'get']) !!}
                    {!! Form::text('filtro_name',null,array('required' => 'required', 'placeholder' => 'Autor o Titulo')) !!}
                    {!! Form::submit('Buscar',['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                    <a href="/nota/crear">Crear Nota</a> | 
                    @if(Auth::guest())
                        <a href="/login">Login</a> |
                        <a href="/register">Registrarse</a> |
                    @endif
                    <a href="/nota/consultar">Consultar notas</a> | 
                    <a href="/nota/propias">Administar  mis notas</a> | 
                </center>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-11">
                @if($errors->any())
                    <center>{{$errors->first()}}</center>   
                @endif  
                <p>
                <div class="card">
                    <div class="buscador">
                        Ordenar por 
                        {!! Form::open(['route' => 'nota.order', 'method' => 'get']) !!}
                        

                        {!! Form::select('type',array(
                            'title' => 'Titulo',
                            'name' => 'Autor',
                            'clasification' => 'Clasificacion',
                            'created_at' => 'Fecha'
                        )) !!}

                        Forma

                        {!! Form::select('form',array(
                            'asc' => 'Ascendente',
                            'desc' => 'Descendente'
                        )) !!}


                        Mostrar

                        {!! Form::radio('show','all',true) !!} Todas las notas

                        {!! Form::submit('Ordenar',['class' => 'btn btn-primary']) !!}
                        
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>   
            <div class="col-md-11">
                @if($notas->isEmpty())
                    <center>No hay ninguna nota creada, <a href="/nota/crear">Crear una nota</a></center>
                @endif
                @foreach($notas as $nota)
			    
                <p>
                <a href="" name='{{$nota->id}}'></a>

                <div class="card">
                    <div class="tableheader"><b>Titulo:</b> {{$nota->title}}, <b>Autor:</b> {{$nota->user->name}} ,<b>Clasificacion</b> {{$nota->clasification}}</div>
                    <!-- Introducir fecha en el header -->
                        <div class="tablebody">
						    <p>   
						    {{$nota->content}}
                            @if(isset($nota->comment))
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="tableheader">Comentario</div>
                                    <div class="card-body">{{$nota->comment}}</div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="buscador">
                            <b>Like</b>:<font color=green> {{$nota->countLikes()}}</font> <br>
                            <b>Dislike</b>:  <font color=red>{{$nota->countDislikes()}}</font></br>
                            @if(!Auth::guest())
                                {!! Form::open(['route' => 'vote', 'method' => 'get']) !!}
                                
                                
                                {!! Form::hidden('note_id',$nota->id) !!}
                                
                                {!! Form::hidden('user_id',Auth::user()->id) !!}
                                
                                {!! Form::hidden('vote',1) !!}
                                
                                {!! Form::hidden('notaUserId',$nota->user->id) !!}
                                
                                {!! Form::submit( 'Like', ['class' => 'likebutton', 'name' => 'vote_type', 'value' => 'like'])!!}

                                {!! Form::submit( 'Dislike', ['class' => 'dislikebutton', 'name' => 'vote_type', 'value' => 'dislike'])!!}

                               

                                {!! Form::close() !!}
                            @endif
                            @if($errors->any())
                                <b><div class ="msgerror">{{$errors->first($nota->id)}}</div></b>
                            @endif
                            <p><p>  
                            <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="Compartir Notas en" data-url="http://localhost:8000/nota/consultar/{{$nota->id}}" data-lang="es" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            <p>
                           <div class="g-plus" data-action="share" data-href="http://localhost:8000/nota/consultar"></div>
                            <p>   
                        </div>
                    </div>
                    @endforeach
                    <p>
                    <div class="card">
                    <div class="card-header">
                    {{$notas->links("pagination::bootstrap-4")}}
                    </div>
                </div>
            </p>
        </div>
    </div>
</div>
@endsection
