<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Nota routes
Route::group(['middleware' => 'auth'], function(){




Route::get('/nota/crear', ['as' => 'nota.crear','uses' => 'NotasController@VistaCrearNota']);

Route::post('/nota/insertar',['as' => 'nota.insertar' ,'uses' => 'NotasController@InsertarNota']);
 
Route::get('/nota/editar',['as' => 'nota.editar', 'uses' => 'NotasController@VistaEditarNota']); 

Route::post('/nota/editar/actualizar', ['as' => 'nota.editar.actualizar','uses' => 'NotasController@ActualizarNota']);

Route::get('/nota/eliminar', ['as' => 'nota.eliminar', 'uses' => 'NotasController@VistaEliminarNota']);

Route::delete('/nota/eliminada' , ['as' => 'nota.eliminada', 'uses' => 'NotasController@EliminarNota']);

Route::get('/nota/propias',['as' => 'nota.propias', 'uses' => 'NotasController@ConsultarNotasPropias']);




Route::get('/vote',['as' => 'vote','uses' => 'NotasController@vote']);

Route::get('nota/consultar/{id}','NotasController@ConsultarIndividual');


});


Route::get('/home', ['as' => 'home', 'uses' => 'NotasController@Index']);

Route::get('/', 'NotasController@Index');

Route::get('/nota/consultar', ['as' => 'nota.consultar','uses' => 'NotasController@ConsultarNotas']);

Route::get('/nota/filtro', ['as' => 'nota.filtro', 'uses' => 'NotasController@ConsultaFiltrada']);	

Route::get('/nota/order',['as' => 'nota.order', 'uses' => 'NotasController@OrdenarNotas']);

Auth::routes();


Route::get('/publicas', 'NotasController@publicas');
Route::get('/publicas/{id}/ver/',['as' => 'publicas.ver', 'uses' => 'NotasController@publicasVer'] );



